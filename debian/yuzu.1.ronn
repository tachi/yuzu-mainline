yuzu(1) -- Nintendo Switch Emulator
===================================

## DESCRIPTION

`yuzu` is an experimental open-source emulator for the Nintendo Switch from the creators of Citra.

It is written in C++ with portability in mind, with builds actively maintained for Windows and Linux.

## SEE ALSO

yuzu-cmd(1)
