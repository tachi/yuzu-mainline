Source: yuzu-mainline
Section: games
Priority: optional
Maintainer: Andrea Pappacoda <andrea@pappacoda.it>
Build-Depends: debhelper-compat (= 13),
               cmake (>= 3.15)
Build-Depends-Arch: catch2 (>= 2.13.7) <!nocheck>,
                    glslang-tools,
                    libavcodec-dev,
                    libavutil-dev,
                    libboost-context-dev,
                    libboost-dev,
                    libcpp-httplib-dev,
                    libcubeb-dev,
                    libdynarmic-dev,
                    libfmt-dev (>= 8.0.1),
                    libinih-dev,
                    liblz4-dev (>= 1.8),
                    libmbedtls-dev,
                    libmicroprofile-dev,
                    libopus-dev,
                    libsdl2-dev,
                    libswscale-dev,
                    libusb-1.0-0-dev,
                    libva-dev,
                    libxbyak-dev,
                    libzstd-dev (>= 1.4),
                    nlohmann-json3-dev (>= 3.8),
                    pkg-config,
                    qtbase5-dev,
                    qtbase5-private-dev,
                    spirv-headers,
                    zlib1g-dev (>= 1.2)
Standards-Version: 4.6.0
Homepage: https://yuzu-emu.org
Vcs-Git: https://salsa.debian.org/debian/yuzu-mainline.git
Vcs-Browser: https://salsa.debian.org/debian/yuzu-mainline
Rules-Requires-Root: no

Package: yuzu
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: Nintendo Switch Emulator
 yuzu is an experimental open-source emulator for the Nintendo Switch
 from the creators of Citra.
 .
 It is written in C++ with portability in mind, with builds actively
 maintained for Windows and Linux.
