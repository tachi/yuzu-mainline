#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=+all qa=+all
# Added by qa=bug, triggers some false positives
export DEB_CXXFLAGS_MAINT_STRIP = -Werror=array-bounds

# -march=x86-64-v2 improves performance on CPUs that support it, but makes
# it impossible to run the binary on really old CPUs supported by Debian.
# I've decided to enable it because CPUs that do not support x86-64-v2
# can't really run yuzu decently anyway, and it improves the experience for
# the ones who can actually run it.
# See https://yuzu-emu.org/help/quickstart/#hardware-requirements
# and https://github.com/yuzu-emu/yuzu/pull/7497
# FMA isn't required, see https://github.com/yuzu-emu/yuzu/pull/4541
export DEB_CFLAGS_MAINT_APPEND = -march=x86-64-v2
export DEB_CXXFLAGS_MAINT_APPEND = -march=x86-64-v2

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
  test := true
else
  test := false
endif

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
		-DCMAKE_INSTALL_BINDIR=games \
		-DYUZU_USE_BUNDLED_SDL2=false \
		-DYUZU_USE_EXTERNAL_SDL2=false \
		-DENABLE_QT_TRANSLATION=false \
		-DYUZU_USE_BUNDLED_QT=false \
		-DYUZU_USE_BUNDLED_BOOST=false \
		-DYUZU_USE_BUNDLED_LIBUSB=false \
		-DYUZU_USE_BUNDLED_FFMPEG=false \
		-DYUZU_USE_BUNDLED_OPUS=false \
		-DYUZU_USE_BUNDLED_XBYAK=false \
		-DYUZU_USE_BUNDLED_INIH=false \
		-DYUZU_USE_BUNDLED_CUBEB=false \
		-DYUZU_USE_BUNDLED_DYNARMIC=false \
		-DYUZU_USE_BUNDLED_HTTPLIB=false \
		-DSIRIT_USE_SYSTEM_SPIRV_HEADERS=true \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INTERPROCEDURAL_OPTIMIZATION=ON \
		-DCMAKE_POLICY_DEFAULT_CMP0069=NEW
